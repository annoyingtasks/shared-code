import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

repositories {
    mavenCentral()
}

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("jacoco")
    id("org.jetbrains.dokka")
}

jacoco.toolVersion = buildVersions.PluginVersion.jacoco

val jacocoReport by tasks.creating(JacocoReport::class) {
    dependsOn("jvmTest")
    group = "Reporting"
    description = "Generate Jacoco coverage reports."
    val coverageSourceDirs = arrayOf(
        "src/main/kotlin"
    )
    val classFiles = File("${buildDir}/classes/kotlin/jvm/")
        .walkBottomUp()
        .toSet()
    classDirectories.setFrom(classFiles)
    sourceDirectories.setFrom(files(coverageSourceDirs))
    additionalSourceDirs.setFrom(files(coverageSourceDirs))

    executionData.setFrom(files("${buildDir}/jacoco/jvmTest.exec"))
    reports {
        xml.required.set(false)
        csv.required.set(false)
        html.required.set(true)
        html.outputLocation.set(file("${buildDir}/reports/jacocoHtml"))
    }

    val jacocoTask = this
    tasks.check {
        dependsOn(jacocoTask)
    }
}

kotlin {
    jvm()
    js(IR) {
        browser {
            testTask {
                enabled = System.getProperty("os.name").lowercase(Locale.getDefault()).contains("win")
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            kotlin.srcDirs("${project.projectDir}/src/generated/kotlin", "${project.projectDir}/src/main/kotlin")
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${buildVersions.DependencyVersions.kotlinSerializationVersion}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${buildVersions.DependencyVersions.kotlinTimeVersion}")
            }
        }
        val commonTest by getting {
            kotlin.srcDirs("${project.projectDir}/src/test/kotlin")
            dependencies {
                implementation(kotlin("test"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        jvm().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
            }
        }
        jvm().compilations["test"].defaultSourceSet {
            dependencies {
                implementation("org.amshove.kluent:kluent:${buildVersions.DependencyVersions.kluentVersion}")
            }
        }
        js().compilations["main"].defaultSourceSet {
            dependencies {
                implementation(kotlin("stdlib-js"))
            }
        }

        configure(listOf(metadata(), jvm(), js())) {
            mavenPublication {
                val targetPublication = this@mavenPublication
                tasks.withType<AbstractPublishToMaven>().matching { it.publication == targetPublication }
            }
        }
    }
}

val generateSources: Task by tasks.creating {
    outputs.dir("${project.projectDir}/src/generated/kotlin")

    val packageName = project.name.split("-").joinToString(separator = "") {
        it.capitalize()
    }.decapitalize()

    doFirst {
        generateFile(
            "${project.group}.${packageName}.info",
            "Version.kt",
            """object Version{
    const val BUILD_VERSION = "${project.version}"
    const val BUILD_DATE = "${Date()}"
    const val BUILD_TIMESTAMP = ${Date().time}
}
"""
        )
    }
}

fun generateFile(
    packageName: String,
    fileName: String,
    fileContent: String
) {
    val outputDir: File = file(
        "${project.projectDir}/src/generated/kotlin/${
            packageName.replace(
                ".",
                "/"
            )
        }"
    )
    if (!outputDir.exists()) outputDir.mkdirs()

    Files.write(
        Paths.get(
            outputDir.absolutePath,
            fileName
        ),
        """package $packageName

$fileContent
""".toByteArray()
    )
}

tasks.compileKotlinMetadata {
    dependsOn(generateSources)
}