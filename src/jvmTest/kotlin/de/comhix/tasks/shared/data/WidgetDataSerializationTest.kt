package de.comhix.tasks.shared.data

import kotlinx.datetime.Clock.System
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.amshove.kluent.shouldBeEqualTo
import kotlin.test.Test
import kotlin.time.Duration.Companion.days

/**
 * @author Benjamin Beeker
 */
class WidgetDataSerializationTest {

    private val jsonSerializer = Json {
        serializersModule = SharedCodeJsonModule.module
    }

    @Test
    fun marshallCalendarData() {

        // given
        val data: WidgetData = CalendarData(
            7,
            listOf(
                CalendarEntry("Name", System.now(), System.now()),
                CalendarEntry("Foo", System.now(), System.now().plus(1.days))
            )
        )

        // when
        val json = jsonSerializer.encodeToString(data)
        val deserialized = jsonSerializer.decodeFromString<WidgetData>(json)

        // then
        deserialized shouldBeEqualTo data
    }
}