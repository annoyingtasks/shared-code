package de.comhix.tasks.shared.api

import de.comhix.tasks.shared.data.Dashboard

/**
 * @author Benjamin Beeker
 */
interface DashboardApi {
    suspend fun get(id: String): Dashboard
}