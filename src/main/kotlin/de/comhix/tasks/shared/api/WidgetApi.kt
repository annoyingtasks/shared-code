package de.comhix.tasks.shared.api

import de.comhix.tasks.shared.data.WidgetData

/**
 * @author Benjamin Beeker
 */
interface WidgetApi {
    suspend fun getData(id: String): WidgetData
}