package de.comhix.tasks.shared.data

import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
open class Dashboard(open val widgets: List<Widget>) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Dashboard

        if (widgets != other.widgets) return false

        return true
    }

    override fun hashCode(): Int {
        return widgets.hashCode()
    }
}