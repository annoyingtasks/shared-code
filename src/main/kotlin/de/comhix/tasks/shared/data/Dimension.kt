package de.comhix.tasks.shared.data

import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
open class Dimension(val x: UInt, val y: UInt, val width: UInt, val height: UInt) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Dimension

        if (x != other.x) return false
        if (y != other.y) return false
        if (width != other.width) return false
        if (height != other.height) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + width.hashCode()
        result = 31 * result + height.hashCode()
        return result
    }
}