package de.comhix.tasks.shared.data

import kotlinx.serialization.modules.SerializersModule

/**
 * @author Benjamin Beeker
 */
object SharedCodeJsonModule {
    val module = SerializersModule {
        polymorphic(WidgetData::class, CalendarData::class, CalendarData.serializer())
    }
}