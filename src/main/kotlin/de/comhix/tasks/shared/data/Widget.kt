@file:Suppress("unused")

package de.comhix.tasks.shared.data

import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

/**
 * @author Benjamin Beeker
 */
@Serializable
open class Widget(val id: String, val widgetType: WidgetType, val dimension: Dimension) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Widget

        if (id != other.id) return false
        if (widgetType != other.widgetType) return false
        if (dimension != other.dimension) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + widgetType.hashCode()
        result = 31 * result + dimension.hashCode()
        return result
    }
}

interface WidgetData

@Serializable
open class CalendarData(val daysToDisplay: Int, val dates: List<CalendarEntry>) : WidgetData {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as CalendarData

        if (daysToDisplay != other.daysToDisplay) return false
        if (dates != other.dates) return false

        return true
    }

    override fun hashCode(): Int {
        var result = daysToDisplay
        result = 31 * result + dates.hashCode()
        return result
    }
}

@Serializable
data class CalendarEntry(val name: String, val start: Instant, val end: Instant) {
    val fullDay: Boolean by lazy {
        (end - start).inWholeDays > 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as CalendarEntry

        if (name != other.name) return false
        if (start != other.start) return false
        if (end != other.end) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + start.hashCode()
        result = 31 * result + end.hashCode()
        return result
    }
}

@Serializable
enum class WidgetType {
    TODO_LIST,
    CALENDAR,
    NOTE
}